<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "queries".
 *
 * @property int $id
 * @property int $sum
 * @property float|null $comission
 * @property int|null $order_number
 */
class Query extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'queries';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sum', 'order_number', 'comission'], 'required'],
            [['sum', 'order_number'], 'default', 'value' => null],
            [['sum', 'order_number'], 'integer'],
            [['comission'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sum' => 'Sum',
            'comission' => 'Comission',
            'order_number' => 'Order Number',
        ];
    }
}
