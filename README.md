Порядок запуска и установки:<br>
1. git clone https://gitlab.com/zmei123123/producer
2. меняем доступы к БД(сonfig/db.php)
3. из папки с проектом запускаем "composer install"
4. из папки с проектом запускаем "php yii migrate/up"
5. для запуска сервиса выполняем "php yii cli/load {url}" - {url} - эндпоинт консамера(http://localhost/consumer/web/index.php?r=api/load для примера)