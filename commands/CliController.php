<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use app\models\Query;
use linslin\yii2\curl\Curl;
use Yii;
use yii\console\Controller;
use yii\console\ExitCode;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class CliController extends Controller
{
    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     * @return int Exit code
     */
    public function actionLoad()
    {
        if (empty($_SERVER['argv'][2])) {
            return false;
        }
        $consumerUrl = $_SERVER['argv'][2];//'http://localhost/consumer/web/index.php?r=api/load';
        while (true) {
            $queries = [];
            for ($i = 1; $i < rand(2, 10); $i++) {
                $query = new Query([
                    'sum' => rand(10, 500),
                    'comission' => rand(5, 20) / 100,
                    'order_number' => rand(1, 20)
                ]);
                if (!$query->save()) {
                    Yii::info('Error saving' . print_r($query->getErrors()));
                    break;
                }
                $queries[] = $query->toArray();
            }
            echo 'Сгенерировали данные' . "\n";
            if (!empty($queries)) {
                $curl = new Curl();
                $signToken = md5('test');
                $result = $curl->setOption(CURLOPT_POSTFIELDS, json_encode($queries))->setHeader('Authorization',
                    $signToken)->post($consumerUrl);
                echo 'Отправляем данные, результат: ' . $result . "\n";
            }
            sleep(20);
        }
    }
}
