<?php

use yii\db\Migration;

/**
 * Class m201209_125634_init
 */
class m201209_125634_init extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('queries', [
            'id' => $this->primaryKey(),
            'sum' => $this->integer()->null(),
            'comission' => $this->double()->null(),
            'order_number' => $this->integer()->null(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201209_125634_init cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201209_125634_init cannot be reverted.\n";

        return false;
    }
    */
}
